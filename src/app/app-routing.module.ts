import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteUrls } from './shared/route-urls';
import { MainLayoutComponent } from './pages/main-layout/main-layout.component';
import { PageHomeComponent } from './pages/page-home/page-home.component';
import { PageAdminComponent } from './pages/page-admin/page-admin.component';

const routes: Routes = [
  {
    path: RouteUrls.Default, component: MainLayoutComponent,
    children: [
      { path: RouteUrls.Default, component: PageHomeComponent },
      { path: RouteUrls.Admin, component: PageAdminComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
