export class Item {
  id: number;
  xCoordinate: string;
  yCoordinate: string;
  color: string;
  width: string;
}

export class ItemManagement {
  id: number;
  xCoordinate = '';
  yCoordinate = '';
  color: string;
  width: string;

  constructor() {}

  static createObjectFrom(item: any): ItemManagement {

    const image = new ItemManagement();

    image.xCoordinate = item.xCoordinate;
    image.yCoordinate = item.yCoordinate;
    image.color = item.color;
    image.width = item.width;

    return item;
  }
}
