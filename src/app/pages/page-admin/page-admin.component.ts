import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ItemService } from '../../services/item.service';
import { ItemManagement } from '../../models/item';

@Component({
  selector: 'app-page-admin',
  templateUrl: './page-admin.component.html',
  styleUrls: ['./page-admin.component.scss']
})
export class PageAdminComponent implements OnInit {
  newItemFormGroup: FormGroup;

  constructor(private itemService: ItemService,
              private formBuilder: FormBuilder,
              private location: Location) { }

  ngOnInit() {
    this.newItemFormGroup = this.formBuilder.group({
      xCoordinate: ['', [Validators.required]],
      yCoordinate: ['', [Validators.required]],
      color: ['', [Validators.required]],
      width: ['', [Validators.required]]
    });
  }

  addItem(): void {
    const newItem = ItemManagement.createObjectFrom(
      this.newItemFormGroup.value
    );
    this.itemService.addItem(newItem)
      .subscribe(response => {
        console.log('addItem response > ', response);
      });
    this.goBack();
  }

  goBack(): void {
    this.location.back();
  }

}
