import { Component, OnInit } from '@angular/core';
import { Item, ItemManagement } from '../../models/item';
import { ItemService } from '../../services/item.service';
import { ModalService } from '../../services/modal.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-page-home',
  templateUrl: './page-home.component.html',
  styleUrls: ['./page-home.component.scss']
})
export class PageHomeComponent implements OnInit {
  items: Item[];
  item: Item;
  itemFormGroup: FormGroup;

  constructor(
    private itemService: ItemService,
    private formBuilder: FormBuilder,
    private modalService: ModalService) { }

  ngOnInit() {
    this.getItems();

    this.itemFormGroup = this.formBuilder.group({
      id: [''],
      xCoordinate: ['', [Validators.required]],
      yCoordinate: ['', [Validators.required]],
      color: ['', [Validators.required]],
      width: ['', [Validators.required]]
    });
  }

  getItems(): void {
    this.itemService.getItems()
      .subscribe(images => this.items = images);
  }

  removeItem(item: Item): void {
    if (confirm(`Are you sure you want delete item with id: ` + item.id + `?`)) {
      this.items = this.items.filter(h => h !== item);
      this.itemService.removeItem(item).subscribe();
    }
  }

  updateItem(): void {
    const newItem = ItemManagement.createObjectFrom(
      this.itemFormGroup.value
    );
    this.itemService.updateItem(newItem)
      .subscribe(response => response);
    this.getItems();
    this.closeModal('edit-item');
  }

  openModal(id: string, item): void {
    this.setDefaultValues(item);
    this.modalService.open(id);
  }

  setDefaultValues(item: Item): void {
    this.itemFormGroup.controls.id.setValue(item.id);
    this.itemFormGroup.controls.xCoordinate.setValue(item.xCoordinate);
    this.itemFormGroup.controls.yCoordinate.setValue(item.yCoordinate);
    this.itemFormGroup.controls.color.setValue(item.color);
    this.itemFormGroup.controls.width.setValue(item.width);
  }

  closeModal(id: string): void {
    this.modalService.close(id);
  }
}
