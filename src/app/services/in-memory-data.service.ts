import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Injectable } from '@angular/core';
import { Item } from '../models/item';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const items = [
      {
        id: 1,
        xCoordinate: '0',
        yCoordinate: '0',
        color: '#4286f4',
        width: '10'
      },
      {
        id: 2,
        xCoordinate: '20',
        yCoordinate: '20',
        color: '#000000',
        width: '100'
      },
      {
        id: 3,
        xCoordinate: '100',
        yCoordinate: '100',
        color: '#000000',
        width: '100'
      }
    ];
    return { items };
  }
}
